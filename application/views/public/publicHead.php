<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$PHP_SELF=$_SERVER['PHP_SELF'] ? $_SERVER['PHP_SELF'] : $_SERVER['SCRIPT_NAME'];
$PHP_SELF = substr($PHP_SELF,1);
//根路径
$BASE_PATH='http://'.$_SERVER['HTTP_HOST'].'/'.substr($PHP_SELF,0,strpos($PHP_SELF, '/')+1);
//动态路由php路径
$BASE_PHP_PATH='http://'.$_SERVER['HTTP_HOST'].'/'.substr($PHP_SELF,0,strrpos($PHP_SELF, '/')+1);
?>
<!-- 引入样式 -->
<link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-default/index.css">
<!-- 先引入 Vue -->
<script src="https://unpkg.com/vue/dist/vue.js"></script>
<!-- 引入组件库 -->
<script src="https://unpkg.com/element-ui/lib/index.js"></script>


<script>
	var basePath = '<?php echo $BASE_PATH ?>';
	var basePhpPath = '<?php echo $BASE_PHP_PATH ?>';
</script>