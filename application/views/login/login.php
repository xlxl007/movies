<?php
defined('BASEPATH') OR exit('No direct script access allowed');


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>test</title>

	<?php  include(APPPATH.'views/public/publicHead.php'); ?>


</head>
<body>
<div id="app">
	<el-button @click="visible = true">按钮</el-button>
	<el-dialog v-model="visible" title="Hello world">
		<p>欢迎使用 Element</p>
	</el-dialog>
</div>

</body>
<script>
	new Vue({
		el: '#app',
		data: function() {
			return { visible: false }
		}
	})
</script>
</html>